/**
* Halmos' Handshake Puzzle.
* http://docs.law.gwu.edu/facweb/jsiegel/Personal/math/mathhome.htm#halmos
*/

vocabulary V {
    type Person isa int
    ShakesHands(Person,Person)
    Married(Person,Person)
    Halmos:Person
    HalmosWife:Person
}

theory T:V{
    // Marriage is a symmetric relation
    !p1 p2: Married(p1,p2) <=> Married(p2,p1).
    // Marriage is anti-reflexive
    !p: ~Married(p,p).
    // At the party, each person was married to exactly one other person
    !p1: ?1 p2: Married(p1,p2).
    // Halmos is married to his wife
    Married(Halmos,HalmosWife).

    // Hands are shaken symmetrically
    !p1 p2: ShakesHands(p1,p2) <=> ShakesHands(p2,p1).
    // Handshaking is anti-reflexive
    !p: ~ShakesHands(p,p).
    // Married people don't shake hands
    !p1 p2: Married(p1,p2) => ~ShakesHands(p1,p2).

    // Halmos identifies each other person by the number of hands he/she shook
    // So for every person except Halmos, the number identifying the person is also
    // the number of persons he/she shook hands with.
    !p[Person]: p~=Halmos => #{p2:ShakesHands(p,p2)}=p.
}

structure S:V{
    Person={0..9}
    Halmos=9
}

procedure main(){
    stdoptions.nbmodels=0
    models=modelexpand(T,S)
    print(#models)
    print(models[1])
}