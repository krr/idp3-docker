<h3>Halmos' Handshake Puzzle</h3>
<p>This excellent puzzle was created by mathematician Paul Halmos, and shows how nicely one can translate knowledge written in natural language to logic.

Halmos and his wife attended a dinner party also attended by four other couples. During the cocktail hour, some of those present shook hands, but in an unsystematic way, with no attempt to shake everyone's hand. Of course, no one shook his or her own hand, no one shook hands with his or her spouse, and no one shook hands with the same person more than once.

During dinner, Halmos asked each of the nine other people present (including his own wife), how many hands that person had shaken. Under the given conditions, the possible answers ranged from 0 to 8. Halmos was interested to notice that each person gave a different answer: one person hadn't shaken anyone's hand, one person had shaken exactly one other's hand, one had shake exactly two hands, and so on, up to one person who had shaken hands with all the others present, except his or her spouse, that is, 8 handshakes.

Now, the question is this: how many hands did Halmos' wife shake?

More information on this puzzle can be found <a href="http://docs.law.gwu.edu/facweb/jsiegel/Personal/math/mathhome.htm#halmos">here</a>.
</p>
