<h3>N-Queens</h3>
<p>The N-Queens problem consists of placing n queens on a n x n board in such a way that no queen can attack another, following the rules of chess. The vocabulary V consists of a two types (diag and index), one constant (n), one predicate (queen), and two functions (diag1 and diag2). The structure S specifies the size of the n x n board and the diag and index types that follow from this. The predicate queen(index, index) represents where the queens are placed on the board. The function diag1(index, index) : diag maps each square on the board to its upper-left-to-lower-right diagonal. The function diag2(index, index) : diag is defined similarly for the lower-left-to-upper-right diagonals.</p>
<pre class="idp-code">
vocabulary V {
    type index isa int
    queen(index, index)
    n : index
    type diag isa int
    diag1(index, index) : diag
    diag2(index, index) : diag
}

structure S : V {
    index = {1..4}
    diag = {1..7}
    n = 4
}
</pre><p>The definitions for diag1 and diag2 are placed in theory T and are given between "{" and "}". They are a special kind of definition: all symbols that they depend (in this case: the constant n and the functions + and -) on are completely known. This means that the definition can be calculated, which is more efficient. The theory also specififies the constraints expressing that the placed queens cannot attack each other. The first constraint expresses that there is exactly one queen on each column of the board: for every x of type index (!x[index]), there is exactly one y of type index (?=1y[index]) for which queen(x, y) holds. The quantified variables are typed in this constraint. This typing is optional and has been omitted in the other constraints as IDP can derive types from the information in the vocabulary. The second constraint expresses that there is exactly one queen on each row of the board. The third constraint expresses that for each diagonal (!d) there are less than two elements in the set {x y : queen(x, y) &amp; diag1(x, y) = d}; the set of elements (x, y) such that they satisfy queen(x, y) &amp; diag1(x, y) = d. This ensures that there is no more than one queen on each upper-left-to-lower-right diagonal. The fourth constraint expresses the same for the lower-left-to-upper-right diagonals.</p>
<pre class="idp-code">
theory T : V {
    { diag1(x, y) = x - y + n. }
    { diag2(x, y) = x + y - 1. }
    !x[index] : ?=1y[index] : queen(x, y).                  <span class="idp-comment">//(1)</span>
    !y : ?=1x : queen(x, y).                                <span class="idp-comment">//(2)</span>
    !d : #{x y : queen(x, y) &amp; diag1(x, y) = d} &lt; 2.        <span class="idp-comment">//(3)</span>
    !d : #{x y : queen(x, y) &amp; diag2(x, y) = d} &lt; 2.        <span class="idp-comment">//(4)</span>
}
</pre><p>The Lua procedure can then be used to retrieve all models and print them. Here we also introduce the calculatedefinitions method that calculates all definitions that are able to be calculated beforehand, such as the definitions for diag1/2 and diag2/2 in this example. These two definitions can be calculated in advance because they each depend on no other predicate that cannot be calculated in advance. We also introduce the option to use XSB to do this calculation. Note that by default, this option is set to false.</p>