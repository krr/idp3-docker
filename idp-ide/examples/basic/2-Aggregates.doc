<h3>Aggregates</h3>
<p>In this example, we show the various aggregates that IDP allows. First, we define a predicate P with 1 argument of type num, which is an integer.<br />
We also define a type sumnum, which is an integer as well, but allows larger values in its eventual range.</p>
<p>Next, we introduce constants for the results of each of the aggregate expressions that IDP allows:</p>
<ul>
<li>Pnr for cardinality,</li>
<li>Psum for the sum aggregate,</li>
<li>Pmin for the minimal element,</li>
<li>Pmax for the maximal element, and</li>
<li>Pprod for the product aggregate.</li>
</ul>
<p>We introduce one last constant, Pnest, to show how IDP supports arbitrary nesting of aggregates.</p>