/**
 * This is a visualisation example game. 
 * The goal of the game is to make all squares green by clicking on them.
 * When a square is clicked, the state of it and it's direct neighbours are flipped.
 * The game stops when all squares are green and the onWin procedure is called.
 * Clicking on the text will stop the program and show the last valid state.
 */

/**
 * Include the original LTC vocabulary and the drawing library.
 */
include "idpd3/idpd3_voc.idp"

/**
 * The vocabulary part of the LTC theory.
 */

/**
 * The basic definition of the types, timeless predicates(initial state, helper functions) and LTC predicates.
 */
vocabulary V_types {
    type L isa int
    //LTC
    type Time
    Start : Time 
    Next(Time) : Time
    
    I_P(L)
}

/**
 * The actions that can be applied to the LTC-state.
 */
LTCvocabulary V_action {
    extern vocabulary V_types

    //Actions
    Click(Time, L)
}

/**
 * The definition of the LTC-state. 
 * This does not include the actions that are externally applied.
 */
LTCvocabulary V_state {
    extern vocabulary V_types
    //Fluents
    P(Time, L)
}

/**
 * The full LTC-vocabulary over which is progressed.
 * This is the current state together with the applied action(s).
 */
LTCvocabulary V {
    extern vocabulary V_state
    extern vocabulary V_action
}
/**
 * This is the visualisation file. 
 * This includes the visualisation vocabularies and theories.
 */

/**
 * The basic vocabulary the library interacts with.
 * This is the d3 vocabulary and the single state LTC vocabulary(V_ss).
 * V_ss includes the current state and the chosen action.
 */
vocabulary V_d3 {
    extern vocabulary idpd3::V_types
    extern vocabulary V_ss
 
    toKey(L) : key
}

/**
 * This is the vocabulary for the transformation of raw actions to interpreted actions.
 * The library does a modelexpand with as structure the union of the current two-valued state and the two-valued input.
 * The actions are fully unknown and will be interpreted by the theory.
 * The function signature is : T:V_d3_in -> S:V_state -> S:idpd3::V_in -> S:V_ss
 */
vocabulary V_d3_in {
    extern vocabulary idpd3::V_in
    extern vocabulary V_d3
}

/**
 * This is the vocabulary for the transformation of the current state to a visualisation.
 * The library does a modelexpand with the two-valued state as input and the fully unknown visualisation.
 * The first model is chosen as visualisation.
 */ 
vocabulary V_d3_out {
    extern vocabulary idpd3::V_out
    extern vocabulary V_d3
}

/**
 * The transformation theory that interpretes the actions from the raw input and the current state.
 */
theory D3_GOL : V_d3_in {
    {
        Click(l) <- d3_click(t, toKey(l)).
    }
}

/**
 * The transformation theory that visualises the current state.
 */
theory GOL_D3 : V_d3_out {
    {
        d3_type(1, toKey(l)) = rect. 
        d3_x(1, toKey(l)) = 0+l*4. 
        d3_y(1, toKey(l)) = 0. 
        d3_rect_width(1, toKey(l)) = 4. 
        d3_rect_height(1, toKey(l)) = 4. 
        d3_color(1, toKey(l)) = "green" <- P(l).
        d3_color(1, toKey(l)) = "red" <- ~P(l).
        
        d3_circ_r(t, k) = r <- false.
        d3_text_label(t, k) = c <- false.
    }
    !t : ! x: d3_width(t) = x => ~?y : x<y.
    !t : ! x: d3_height(t) = x => ~?y : x<y.
}
/**
 * This is the normal LTC problem theory, as you would normally write it.
 * (This is assuming you want to do the encoded three-valued actions SetOn, SetOff, SetUnknown)
 */
theory T : V {
    {
        P(Start, l) <- I_P(l).
        P(Next(t), l) <- P(t, l) <=> ~(?l1 : Click(t, l1) & abs(l-l1) =< 1).
    }
    !t:?l : ~P(t, l).
}

procedure toString(el) {
    return tostring(el);
}
/**
 * This is the input data: the LTC-input data and the visualisation constants.
 */
structure Default : V_d3 { 
    //LTC
    L = {1..4}
    I_P = {1}

    //idpd3 visualisation
    time = {1}
    toKey = procedure toString
 
    color = {"red"; "green"}
    width = {0..25}
    height = {0..25}
}

/**
 * The procedure to call when the game stops.
 */
procedure onWin() {
	local win = '{"animation": [{"height":"6","time":1,"elements":'..
  		'[{"y":"1","x":"2","key":"w","color":"blue",'..
        '"text_label":"You won!","type":"text"}],"width":"6"}]}';
  local answer = coroutine.yield(win);
    return nil;
}
/**
 * The main procedure.
 * This procedure first loads the library and sets some optimal options.
 * Then the default structure is cloned and casted to the correct vocabularies.
 * Then the run function is created with the needed aruments:
 *      The LTC-theory, structure and state vocabulary,
 *      The input-theory, structure and vocabulary,
 *      The output-theory, structure and vocabulary,
 *      An optional function f(S) called when no new LTC-state can be found.
 * The function is then run and on exit the last valid state is returned.
 */
procedure main() {
    idpd3.init_idpd3();
    stdoptions.splitdefs = false;
    stdoptions.postprocessdefs = false
    stdoptions.cpsupport = true
    stdoptions.xsb = true    
    
    local S = clone(Default);
    local S_d3_in = clone(Default);
    local S_d3_out = clone(Default);
    setvocabulary(S, V);
    setvocabulary(S_d3_in, V_d3_in);
    setvocabulary(S_d3_out, V_d3_out);

	local coIn = idpd3_browser:createLTC(
        T, S, V_state_ss,
        D3_GOL, S_d3_in, V_d3_in,
        GOL_D3, S_d3_out, V_d3_out, onWin);
    local lastState = coIn();
    
    print("Finished");
    print(lastState);
}
