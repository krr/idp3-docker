vocabulary V{
    type Node
    Impossible(Node,Node) //Some given relation expressing from which
    Edge(Node,Node)
    Reachable(Node,Node)
}


theory T: V{
    //Reachable is defined inductively as follows:
    {
        //If there is an Edge from x to y, than y is reachable from x
        ! x y: Reachable(x,y) <- Edge(x,y).
        //If there is some point between x and y and z is reachable from x, and y from z, than y is reachable from x.
        ! x y: Reachable(x,y) <- ? z: Reachable(x,z) & Reachable(z,y).
    }

    //The graph must be fully connected...
    !x y : Reachable(x,y).

    //...and can have no edges on impossible locations
    ! x y: Edge(x,y) => ~Impossible(x,y).

    //and... more constraints?

}

//some instance without special meaning
structure S:V{
    Node={A..D}
    Impossible = {A,B; B,C; C,D; A,C; B,A; A,A; B,B;C,C;D,D}
}

procedure main(){
    //printmodels and allmodels are methods in the <mx> include that allow us to shorten our code ;-)
    printmodels(allmodels(T,S))
}
