String.prototype.endsWith = function(suffix) {
    return this.indexOf(suffix, this.length - suffix.length) !== -1;
};

$.extend({
    getUrlVars: function(){
      var vars = [], hash;
      var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
      for(var i = 0; i < hashes.length; i++)
      {
        hash = hashes[i].split('=');
        vars.push(hash[0]);
        vars[hash[0]] = hash[1];
      }
      return vars;
    },
    getUrlVar: function(name){
      return $.getUrlVars()[name];
    }
  });

$.fn.setCursorPosition = function(pos) {
  this.each(function(index, elem) {
    if (elem.setSelectionRange) {
      elem.setSelectionRange(pos, pos);
    } else if (elem.createTextRange) {
      var range = elem.createTextRange();
      range.collapse(true);
      range.moveEnd('character', pos);
      range.moveStart('character', pos);
      range.select();
    }
  });
  return this;
};

function saveGist(e){
    var stringData = $.post("./submitGist",getCode())
      .done(function(d){
          d = "http://dtai.cs.kuleuven.be/krr/idp-ide/?src="+d;
          $("#copylink").text(d);
        });

  }

function loadCode(e){
  var stringData = $.post("./getGist",e)
    .done(function(d){
        window.editor.mirror.setValue(d.source);
        console.log("The loaded code was saved by IDP version: "+d.version)
      });

}

function loadUrl(u){
    var stringData = $.ajax({
                    url: u,
                    async: false
                 }).responseText;
     window.editor.mirror.setValue(stringData);
  }


function getCode(){
    return window.editor.mirror.getValue();
}

function evaljson(t){

    var filename = "web.idp"
    if($("#filename").val()){
      filename = $("#filename").val();
    }

    var o =
        {"code" : getCode(),
        "GV" :$('#GV').data('slider').value(),
				"SV" : $('#SV').data('slider').value(),
				"NB" : $('#NB').data('slider').value(),
         "inference" : t,
         "stable" : $("#isStable").prop("checked"),
         "cp" : $("#cp").prop("checked"),
         "filename" : filename,
         "eInput" : ""
   };
    return JSON.stringify(o);
}

function startSpinning(){
  $('#spinner').spin({
        lines: 12, // The number of lines to draw
        length: 10, // The length of each line
        width: 4, // The line thickness
        radius: 5, // The radius of the inner circle
        corners: 1, // Corner roundness (0..1)
        rotate: 0, // The rotation offset
        direction: 1, // 1: clockwise, -1: counterclockwise
        color: '#999', // #rgb or #rrggbb or array of colors
        speed: 1, // Rounds per second
        trail: 60, // Afterglow percentage
        shadow: false, // Whether to render a shadow
        hwaccel: false, // Whether to use hardware acceleration
        className: 'spinner', // The CSS class to assign to the spinner
        zIndex: 2e9, // The z-index (defaults to 2000000000)
        top: '50%', // Top position relative to parent
        left: '50%' // Left position relative to parent
    });
}

function stopSpinning(){
  $('#spinner').text("");
}

function run(){
  myLayout.open('east')
  cons.reset();
  var t = parseInt($("#runOpt").val());
  makeSocket(JSON.parse(evaljson(t)))
}


function hideTooltip(tt) {
    if (!tt.parentNode) return;
    if (tt.style.opacity == null) rm(tt);
    tt.style.opacity = 0;
    setTimeout(600);
}
function showTooltip(e, content) {
    var tt = document.createElement("div");
    var inner = document.createElement("div");
    inner.className = "CodeMirror-lint-message-core";
    tt.className = "CodeMirror-lint-tooltip";
    inner.appendChild(document.createTextNode(content));
    tt.appendChild(inner);
    document.body.appendChild(tt);

    function position(e) {
      if (!tt.parentNode) return CodeMirror.off(document, "mousemove", position);
      tt.style.top = Math.max(0, e.clientY - tt.offsetHeight - 5) + "px";
      tt.style.left = (e.clientX + 5) + "px";
    }
    CodeMirror.on(document, "mousemove", position);
    position(e);
    if (tt.style.opacity != null) tt.style.opacity = 1;
    return tt;
}
function showTooltipFor(e, content, node) {
    var tooltip = showTooltip(e, content);
    function hide() {
      CodeMirror.off(node, "mouseout", hide);
      hideTooltip(tooltip); tooltip = null;
    }
    var poll = setInterval(function() {
      if (tooltip) for (var n = node;; n = n.parentNode) {
        if (n == document.body) return;
        if (!n) { hide(); break; }
      }
      if (!tooltip) return clearInterval(poll);
    }, 400);
    CodeMirror.on(node, "mouseout", hide);
 }
function makemarker(labels){
      var marker = document.createElement("div");
      marker.className = "CodeMirror-lint-marker-core";
      marker.innerHTML = " ";
      CodeMirror.on(marker, "mouseover", function(e) {
      showTooltipFor(e, labels, marker);
      });
      return marker;
}


function showCore(arg){
  if(!arg) {
    return;
  }

  arg.forEach(function(coreLine){
    var lineNr = coreLine.line_nr - 1;
    var charNr = 0;
    var firstToken = window.editor.mirror.getTokenAt(CodeMirror.Pos(lineNr,1));
    if(firstToken.type == "whitespace"){
      charNr = firstToken.end;
    }
    var ann = {};
    ann.from = CodeMirror.Pos(lineNr, charNr);
    ann.to = CodeMirror.Pos(lineNr+1, 0);
    ann.message = coreLine.hasmessage;
    ann.severity = "core";
    coreMarkers.push(
      window.editor.mirror.markText(
      ann.from,
      ann.to,
      {className:"CodeMirror-lint-mark-core",
       __annotation: ann,
      })
    );
    window.editor.mirror.setGutterMarker(lineNr, "core-gutter", makemarker(coreLine.hasmessage));

  });
}

function deriveRoute(){
  route = window.location.pathname.split('/');
  routeStr = ""
  for(var i = 0; i < route.length-1;i++){
    routeStr += route[i] + "/";
  }
  return routeStr
}

function makeSocket(code){
    if(document.socket){
      document.socket.close();
    }
    document.socket = io("",{path: deriveRoute()+"socket/", multiplex : false});
    document.socket.on('std-err',function(d){
          cons.report([{msg:d,
        className:"jquery-console-message-error"}])
        });

    document.socket.on('start',function(d){
              startSpinning();
              cons.enable();
            });
    document.socket.on('stop',function(d){
              stopSpinning();
              cons.disable();
              document.socket.close();
            });

    document.socket.on('std-out',function(d){
      function tryParse(str) {
        try{
          return JSON.parse(str);
        }catch(err){
          return undefined;
        }
      }

      var out = tryParse(d);
      if(out) {
        function setAnimationData(d) {
          window.animation.setData(d);
        }
        function handle(f, name) {
          return function(data) {
            if(data[name]) {
              f(data[name]);
              data[name] = "...";
            }
            return data;
          }
        }
        out = handle(setAnimationData, "animation")(out);
        out = handle(showCore, "core")(out);
         cons.report([{msg:JSON.stringify(out), className: "jquery-console-message"}]);
      } else {
         cons.report([{msg:d, className: "jquery-console-message"}]);
      }
    });
    document.socket.on('start',function(d){
      window.animation.clearData();
      window.editor.mirror.clearGutter("core-gutter");
      coreMarkers.forEach( function(e) {
        e.clear();
      });
      $("#replSymb").click();$("#replSymb").click();
    });

    document.socket.emit("execute",code);
}

function initCommonObjects(){
  $("#symbolCSS")[0].active = true;

  var target = d3.select("#idpd3");
  var animation = new idpd3.Animation(target);
  animation.setIOHandler(function(json){
    var str = JSON.stringify({animation:json});
    document.socket.emit('std-in', str);
  });

  window.animation = animation;
  window.coreMarkers = [];

function makeslider(obj,val,min,max){
  var tooltip = $('<div id="tooltip" />').css({
      position: 'absolute',
      top: -25,
      left: -10
  }).hide();
  obj.slider({
      value: val,
      min: min,
      max: max,
      step: 1,
      slide: function(event, ui) {
          tooltip.text(ui.value);
      },
      change: function(event, ui) {}
  }).find(".ui-slider-handle").append(tooltip).hover(function() {
      tooltip.show()
  }, function() {
      tooltip.hide()
  })
}


makeslider($("#NB"),1,0,10);
makeslider($("#GV"),0,0,5);
makeslider($("#SV"),0,0,5);

    $("#replSymb").click(function(){
         if($("#symbolCSS")[0].active){
           $("#symbolCSS")[0].href  = "";
           $("#symbolCSS")[0].active =false;
         }else{
           $("#symbolCSS")[0].href = "./styles/symbol.css";
           $("#symbolCSS")[0].active = true
         }
    });

    $("#runButton").click(function(){
      run();
    });

    $("#output").addClass("console");
    window.cons = $('#output').console({
      fadeOnReset: false,
      commandValidate:function(line){
        if (line == "") return false;
        else return true;
      },
      commandHandle:function(line,r){
        document.socket.emit("std-in",line);
        cons.report = r;
        return {}
      },
      continuedPrompt: false,
      promptHistory:true
    });
    cons.disable();

}
