var myLayout;
window.addEventListener('HTMLImportsLoaded', function(e) {

    myLayout = $('#ideContainer').layout({
        closable: true, // pane can open & close
        resizable: true, // when open, pane can be resized
        slidable: true, // when closed, pane can 'slide' open over other panes - closes on mouse-out
        livePaneResizing: true,
        west__minSize: 100,
        east__minSize: 100,
        center__minWidth: 100,
        east__size: .33,
        west__size: .33,
        enableCursorHotkey: false
    });


});

function loadExample(e) {
    myLayout.close('east');
    var documentation = $.ajax({
        url: "./examples/" + e + ".html",
        async: false
    }).responseText;
    myLayout.open('west');
    $("#manual").html(documentation);
    setTimeout(function() {
        myLayout.resizeAll();
    }, 500)
}

function save(e) {
    saveGist();
}

function download() {
    var a = document.createElement("a");
    document.body.appendChild(a);
    a.style = "display: none";
    var blob = new Blob([getCode()], {
        type: 'application/octet-stream'
    });
    var url = URL.createObjectURL(blob);
    var d = new Date()
    var s = 'webIDE_' + d.getDate() + '-' + (d.getMonth() + 1) + '-' + (d.getHours()) + 'h' + d.getMinutes() + '.idp'
    a.href = url;
    a.download = s;
    a.click();
}


function setSize() {
    var containerHeight = $(window).height();
    $("#ideContainer").height(containerHeight - 70);
    $(".CodeMirror").height(containerHeight - 100);
    $(".jquery-console-inner").height(containerHeight - 130);

    mh = (window.innerHeight - 100) + "px";
    $("#examples").css({
        "max-height": mh
    });
}


function initExamples(location) {
    $.getJSON(location, function(data) {

        data.forEach(function(element) {
            var divider = $('<li>', {
                class: "divider",
                role: "presentation"
            });
            $("#examples").append(divider);
            var title = $('<li>', {
                role: "presentation",
                class: "dropdown-header"
            });
            title.append(element.title);
            $("#examples").append(title);
            element.examples.forEach(function(example) {
                var link = $('<a>', {
                    href: "javascript:loadExample(\"" + example.file + "\")"
                });

                link.append(example.name);
                var listEl = $('<li>');
                listEl.append(link);
                $("#examples").append(listEl);
            });

        });

    });
}

window.addEventListener('HTMLImportsLoaded', function(e) {
    //INIT EDITOR
    $(window).resize(function() {
        setSize();
    });

    var te = document.getElementById("codeArea");
    var uiOptions = {
        path: 'js/',
        searchMode: 'popup'
    }
    var codeMirrorOptions = {
        mode: "idp",
        matchBrackets: true,
        lineNumbers: true,
        lineWrapping: true,
        extraKeys: {
            "Ctrl-Q": function(cm) {
                cm.foldCode(cm.getCursor());
            },
            "Ctrl-Space": "autocomplete",
            "Ctrl-Enter": function() {
                run();
            },
            "Ctrl-R": function() {
                run();
            },
            "Ctrl-E": function() {
                save();
            },
            "Esc": function(cm) {
                if (cm.getOption("fullScreen")) cm.setOption("fullScreen", false);
            },
            "Shift-Ctrl-F": function() {
                window.editor.reindent();
            },
            "Alt-C": function() {
                $("#replSymb").click();
            }
        },
        foldGutter: true,
        gutters: ["CodeMirror-linenumbers", "core-gutter", "CodeMirror-foldgutter", "CodeMirror-lint-markers"],
        lint: true,
        theme: "elegant",
        styleActiveLine: true,
        autoCloseBrackets: true
    };



    window.editor = new CodeMirrorUI(te, uiOptions, codeMirrorOptions);
    window.editor.options.lint = true;
    window.editor.mirror.state.lint.options.async = true;
    window.editor.mirror.on("change", function() {
        $("#copylink").text("");
        localStorage.code = getCode();
    });



    initCommonObjects();
    initExamples($.getUrlVar('jsonLoc') || "./examples/examples.json");

    setSize();
    myLayout.resizeAll();
    myLayout.close('west')
    myLayout.close('east')
    if (localStorage.replSymb == undefined) {
        localStorage.replSymb = true;
    } else if (localStorage.replSymb == "false") {
        $("#replSymb").click();
    }
    if ($.getUrlVar('src')) {
        loadCode($.getUrlVar('src'))
        localStorage.code = getCode();
        history.pushState(null, "IDP web-IDE", "./");
    } else if ($.getUrlVar('example')) {
        loadUrl("./examples/" + $.getUrlVar('example') + ".idp");
        localStorage.code = getCode();
        history.pushState(null, "IDP web-IDE", "./");
    } else if ($.getUrlVar('present')) {
        loadUrl("https://dtai.cs.kuleuven.be/krr/files/idp_sources/" + $.getUrlVar('present') + ".idp");
        localStorage.code = getCode();
        history.pushState(null, "IDP web-IDE", "./");
    } else if (localStorage.code) {
        window.editor.mirror.setValue(localStorage.code);
    } else {
        loadExample("intro/1-Introduction")
    }

    if ($.getUrlVar('chapter')) {
        loadExample($.getUrlVar('chapter'));
    }

});


(function(i, s, o, g, r, a, m) {
    i['GoogleAnalyticsObject'] = r;
    i[r] = i[r] || function() {
        (i[r].q = i[r].q || []).push(arguments)
    }, i[r].l = 1 * new Date();
    a = s.createElement(o),
        m = s.getElementsByTagName(o)[0];
    a.async = 1;
    a.src = g;
    m.parentNode.insertBefore(a, m)
})(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

ga('create', 'UA-59638041-1', 'auto');
ga('send', 'pageview');
